import React from "react";
import "../App.css";

function Header() {
  function setGreeting() {
    const index = Math.floor(Math.random() * 10);

    const greetings = [
      "Aloha",
      "Hi mate",
      "What's up",
      "Hiiii",
      "Hello",
      "G'day",
      "Peek-a-boo",
      "Howdy-doody",
      "Ahoy",
      "Konnichiwa",
    ];

    const days = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ];

    const emojis = [
      "🙃🥁",
      "😍🥂",
      "😌☕️",
      "😁🍍",
      "😜🍕",
      "🤗🍪",
      "😎🍹",
      "🤠🪃",
      "🥸🎬",
      "😊🍫",
    ];

    const now = new Date();

    const currentDay = days[now.getDay()];

    return (
      <h1 className="header__title">
        {greetings[index]}, it's {currentDay} {emojis[index]}
      </h1>
    );
  }

  return (
    <header className="header">
      <img src="./images/logo.png" alt="logo" className="header__logo" />
      {setGreeting()}
    </header>
  );
}

export default Header;
