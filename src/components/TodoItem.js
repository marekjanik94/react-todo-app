import React from "react";

import "../App.css";

function TodoItem({ todos, completeTodo, removeTodo }) {
  return todos.map((todo, index) => (
    <li className="todo-item">
      <input
        className={
          todo.isComplete ? "todo-item__input completed" : "todo-item__input"
        }
        key={index}
        value={todo.text}
      />
      <button
        className="todo-item__complete"
        key={todo.id}
        onClick={() => completeTodo(todo.id)}
      >
        <i className="fas fa-check"></i>
      </button>
      <button className="todo-item__trash" onClick={() => removeTodo(todo.id)}>
        <i className="fas fa-trash"></i>
      </button>
    </li>
  ));

  // return (
  //   <li className="todo-item">
  //     <input className="todo-item__input" />
  //     <button className="todo-item__complete">
  //       <i className="fas fa-check"></i>
  //     </button>
  //     <button className="todo-item__trash">
  //       <i className="fas fa-trash"></i>
  //     </button>
  //   </li>
  // );
}

export default TodoItem;
