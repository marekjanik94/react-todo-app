import React, { useState } from "react";
import Todo from "./Todo";
import TodoItem from "./TodoItem";

import "../App.css";

function TodoList() {
  const [todos, setTodos] = useState([]);

  const addTodo = (todo) => {
    if (!todo.text || /^\s*$/.test(todo.text)) {
      return;
    }

    const newTodos = [todo, ...todos];

    setTodos(newTodos);
  };

  const removeTodo = (id) => {
    const removeArr = [...todos].filter((todo) => todo.id !== id);

    setTodos(removeArr);
  };

  const completeTodo = (id) => {
    let updatedTodos = todos.map((todo) => {
      if (todo.id === id) {
        todo.isComplete = !todo.isComplete;
      }
      return todo;
    });
    setTodos(updatedTodos);
  };

  return (
    <div className="todo-container">
      <Todo onSubmit={addTodo} />
      <ul className="todo-list">
        <TodoItem
          todos={todos}
          completeTodo={completeTodo}
          removeTodo={removeTodo}
        />
      </ul>
    </div>
  );
}

export default TodoList;
