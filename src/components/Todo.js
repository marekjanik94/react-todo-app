import React, { useState } from "react";
import "../App.css";

function Todo(props) {
  const [input, setInput] = useState("");

  const handleChange = (e) => {
    setInput(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    props.onSubmit({ id: Math.floor(Math.random() * 10000), text: input });

    setInput("");
  };

  return (
    <form className="todo" onSubmit={handleSubmit}>
      <input
        type="text"
        className="todo__input"
        placeholder="✍️ Add task..."
        name="text"
        value={input}
        onChange={handleChange}
      />
      <button className="todo__btn" type="submit">
        <i className="fas fa-plus"></i>
      </button>
    </form>
  );
}

export default Todo;
